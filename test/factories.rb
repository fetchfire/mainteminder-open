FactoryGirl.define do
  
  factory :user do
    name "Test User"
  end
  
  factory :bike, class: User do
    name "Test Bike"
    odo 1
    colour "black"
    plate "abc123"
    year 2000
  end
  
  factory :dataline, class: Bike do
    datatype "odometer"
    value 1000
    recorded_at Time.now
  end
  
end