require 'test_helper'

class CurrentOdoTest < ActiveSupport::TestCase
  test "check current odo" do
    bike = Bike.find(1)
    assert bike.current_odo == 100
  end
end
