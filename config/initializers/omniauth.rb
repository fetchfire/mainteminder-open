Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, '', ''
  provider :google_oauth2, '', ''
  provider :facebook, '', ''
  provider :identity, on_failed_registration: lambda { |env|
      IdentitiesController.action(:new).call(env)
    }
end