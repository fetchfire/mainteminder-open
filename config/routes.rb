Mainteminderx::Application.routes.draw do


  resources :admin_objects


  get "log_out" => "sessions#destroy", :as => "log_out"
  get "delete_item" => "m_items#destroy", :as => "delete_item"
  get "log_in" => "sessions#new", :as => "log_in"
  get "sign_up" => "users#new", :as => "sign_up"
  get "bikes" => "bikes#index", :as => "bikes"

post "versions/:id/revert" => "versions#revert", :as => "revert_version"

root :to => "bikes#index"
get "/auth/:provider/callback" => "sessions#create"
post "/auth/identity/callback" => "sessions#create"
get "/auth/failure" => "sessions#failure" 


resources :users do
	post :set_admin
	post :unset_admin
	post :set_tester
	post :unset_tester
	post :destroy_by_admin
  post :enable_by_admin
end

resources :sessions

resources :identities

resources :bikes do
	get :import
	get :report
  get :dash
  member do
    get :graph
  end
end

resources :datalines do
  collection do
	  post :odo
  end
  
end

resources :pages

resources :m_items do
  post :complete
  collection { post :import }

end

get '/about' => 'pages#about'
get '/faqs' => 'pages#faqs'
get '/changelog' => 'pages#changelog'
get '/login' => 'pages#login'
get '/privacy' => 'pages#privacy'

get '/afaqs' => 'admin_objects#faqs'
get '/changes' => 'admin_objects#changes'
get '/text' => 'admin_objects#text'
get '/stats' => 'admin_objects#stats'

end
