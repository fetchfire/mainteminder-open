default_run_options[:pty] = true
ssh_options[:forward_agent] = true

# set :rvm_ruby_string, :local               # use the same ruby as used locally for deployment
# set :rvm_autolibs_flag, "read-only"        # more info: rvm help autolibs

# before 'deploy:setup', 'rvm:install_rvm'   # install RVM
# before 'deploy:setup', 'rvm:install_ruby'  # install Ruby and create gemset, OR:
# before 'deploy:setup', 'rvm:create_gemset' # only create gemset

# set :rvm_ruby_string, :local        # use the same ruby as used locally for deployment

# before 'deploy', 'rvm:install_rvm'  # update RVM
# before 'deploy', 'rvm:install_ruby' # install Ruby and create gemset (both if missing)

require "rvm/capistrano"

require "bundler/capistrano"
require 'capistrano/ext/multistage'

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

set :application, "mainteminder"
set :scm, :git
set :repository, "git@bitbucket.org:fetchfire/mainteminder.git"
#set :repository, "https://fetchfire@bitbucket.org/fetchfire/mainteminder.git"
set :user, "deploy"





