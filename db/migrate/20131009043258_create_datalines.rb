class CreateDatalines < ActiveRecord::Migration
  def change
    create_table :datalines do |t|
      t.string :type
      t.float :value
      t.datetime :recorded_at
      t.integer :bikeid

      t.timestamps
    end
  end
end
