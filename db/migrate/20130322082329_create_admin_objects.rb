class CreateAdminObjects < ActiveRecord::Migration
  def change
    create_table :admin_objects do |t|
      t.string :name
      t.text :body
      t.date :date_created
      t.string :type
      t.integer :priority
      t.integer :answer

      t.timestamps
    end
  end
end
