class AddDisabledDateUsers < ActiveRecord::Migration
  def change
  	add_column :users, :disabled_at, :datetime
  	add_column :users, :guess_on, :boolean
  	add_column :users, :is_admin, :boolean
  end
end
