class Renametype < ActiveRecord::Migration
  def up
  	add_column :admin_objects, :object_type, :string
  	remove_column :admin_objects, :type
  end

  def down
  end
end
