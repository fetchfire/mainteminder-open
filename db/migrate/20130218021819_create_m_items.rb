class CreateMItems < ActiveRecord::Migration
  def change
    create_table :m_items do |t|
      t.integer :bike_id
      t.string :name
      t.integer :odo
      t.date :date
      t.timestamps
    end
  end
end
