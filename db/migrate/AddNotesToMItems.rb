class AddNotesToMItems < ActiveRecord::Migration
  def up
    change_table :m_items do |t|
      t.number :notes
    end
  end
end