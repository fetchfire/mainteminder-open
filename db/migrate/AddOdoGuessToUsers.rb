class AddOdoGuessToBikes < ActiveRecord::Migration
  def up
    change_table :bikes do |t|
      t.number :odo_guess
    end
  end
end