class ChangeBikeidToBikeId < ActiveRecord::Migration
  def change
      remove_column :datalines, :bikeid
      add_column :datalines, :bike_id, :integer
    end
end
