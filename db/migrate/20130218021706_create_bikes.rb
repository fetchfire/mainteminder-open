class CreateBikes < ActiveRecord::Migration
  def change
    create_table :bikes do |t|
      t.integer :user_id
      t.string :name
      t.string :plate
      t.string :colour
      t.integer :year
      t.integer :odo
       t.string :picurl
      t.timestamps
    end
  end
end
