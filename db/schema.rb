# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131114010524) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_objects", force: true do |t|
    t.string   "name"
    t.text     "body"
    t.date     "date_created"
    t.integer  "priority"
    t.integer  "answer"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "object_type"
  end

  create_table "bikes", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "plate"
    t.string   "colour"
    t.integer  "year"
    t.integer  "odo"
    t.string   "picurl"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "odoguess"
  end

  create_table "datalines", force: true do |t|
    t.string   "type"
    t.float    "value"
    t.datetime "recorded_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "bike_id"
    t.string   "datatype"
  end

  create_table "identities", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "m_items", force: true do |t|
    t.integer  "bike_id"
    t.string   "name"
    t.integer  "odo"
    t.date     "date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "notes"
    t.datetime "completed_at"
    t.integer  "completed_at_odo"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "uid"
    t.string   "provider"
    t.string   "name"
    t.string   "avatar"
    t.datetime "disabled_at"
    t.boolean  "guess_on"
    t.boolean  "is_admin"
    t.boolean  "is_subscribed"
    t.boolean  "is_tester"
    t.string   "userid"
    t.integer  "limit"
  end

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
