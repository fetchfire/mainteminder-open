require 'spec_helper'

describe "datalines/edit" do
  before(:each) do
    @dataline = assign(:dataline, stub_model(Dataline,
      :type => "",
      :value => 1.5,
      :integer => ""
    ))
  end

  it "renders the edit dataline form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", dataline_path(@dataline), "post" do
      assert_select "input#dataline_type[name=?]", "dataline[type]"
      assert_select "input#dataline_value[name=?]", "dataline[value]"
      assert_select "input#dataline_integer[name=?]", "dataline[integer]"
    end
  end
end
