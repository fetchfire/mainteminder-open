require 'spec_helper'

describe "datalines/show" do
  before(:each) do
    @dataline = assign(:dataline, stub_model(Dataline,
      :type => "Type",
      :value => 1.5,
      :integer => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Type/)
    rendered.should match(/1.5/)
    rendered.should match(//)
  end
end
