require 'spec_helper'

describe "datalines/index" do
  before(:each) do
    assign(:datalines, [
      stub_model(Dataline,
        :type => "Type",
        :value => 1.5,
        :integer => ""
      ),
      stub_model(Dataline,
        :type => "Type",
        :value => 1.5,
        :integer => ""
      )
    ])
  end

  it "renders a list of datalines" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
