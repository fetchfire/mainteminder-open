require 'spec_helper'

describe "datalines/new" do
  before(:each) do
    assign(:dataline, stub_model(Dataline,
      :type => "",
      :value => 1.5,
      :integer => ""
    ).as_new_record)
  end

  it "renders new dataline form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", datalines_path, "post" do
      assert_select "input#dataline_type[name=?]", "dataline[type]"
      assert_select "input#dataline_value[name=?]", "dataline[value]"
      assert_select "input#dataline_integer[name=?]", "dataline[integer]"
    end
  end
end
