require "spec_helper"

describe DatalinesController do
  describe "routing" do

    it "routes to #index" do
      get("/datalines").should route_to("datalines#index")
    end

    it "routes to #new" do
      get("/datalines/new").should route_to("datalines#new")
    end

    it "routes to #show" do
      get("/datalines/1").should route_to("datalines#show", :id => "1")
    end

    it "routes to #edit" do
      get("/datalines/1/edit").should route_to("datalines#edit", :id => "1")
    end

    it "routes to #create" do
      post("/datalines").should route_to("datalines#create")
    end

    it "routes to #update" do
      put("/datalines/1").should route_to("datalines#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/datalines/1").should route_to("datalines#destroy", :id => "1")
    end

  end
end
