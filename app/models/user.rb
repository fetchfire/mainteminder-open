class User < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
	has_many :bikes
  has_many :m_items, :through => :bikes
  has_many :datalines, :through => :bikes


  def update_picture(url)
    self.avatar = url 
    save!
  end

  def limit_reached
    @bikes = self.bikes.all
    total = @bikes.count
    if total >= self.limit
      return true
    else 
      return false
    end
  end

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.disabled_at = nil
      user.is_admin = false
      user.guess_on = true
      user.is_subscribed = true
      user.limit = 5
      user.avatar = "/images/user_icon.png"

      # test users can use beta features
      user.is_tester = false

      

      if auth["provider"] == "twitter"
        user.userid = auth["info"]["nickname"]
        user.name = auth["info"]["name"]
        user.email = ""
        user.avatar = auth["info"]["image"]
        
        if user.avatar == nil
          user.avatar = "/images/user_icon.png"
        end
      end
      
      if auth["provider"] = "facebook"
        user.userid = auth["info"]["nickname"]
        user.name = auth["info"]["name"]
        user.email = auth["info"]["email"]
        user.avatar = auth["info"]["image"]
        
        if user.avatar == nil
          user.avatar = "/images/user_icon.png"
        end
      end
      
      if auth["provider"] = "google_oauth2"
        user.userid = auth["info"]["nickname"]
        user.name = auth["info"]["name"]
        user.email = auth["info"]["email"]
        user.avatar = auth["info"]["image"]
        
        if user.avatar == nil
          user.avatar = "/images/user_icon.png"
        end
      end
      
      if auth["provider"] = "identity"
        user.name = auth["info"]["name"]
        user.email = auth["info"]["email"]
        
      end
      
    end
  end

  def overdueCount
    count = 0
    self.bikes.each do  |bike|
      count = count + bike.overcount
    end
    return count
  end

  def dueSoonCount
    count = 0
    self.bikes.each do  |bike|
      count = count + bike.dueSoonCount
    end
    return count
  end

  def self.weekly_mail
    users = User.all
    count = 0
    users.each do  |user|
      if user.id % 7 == (Time.zone.now.wday + 1)
        if user.email != "" && user.email != nil && user.is_subscribed == true && user.disabled_at == nil
          if user.overdueCount > 0 || user.dueSoonCount > 0
            MainDue.due(user).deliver
            count = count + 1
          end
        end
      end
    end
    MainDue.total_sent(count).deliver
  end
  
end
