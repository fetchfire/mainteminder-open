class Dataline < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
  belongs_to :bike
  
  validates :value, :numericality => true
  
	def self.import(file)
    allowed_params = ["datatype", "value", "recorded_at"]
    CSV.foreach(file.path, :headers => true) do |row|
  
	  next if row.header_row?
      begin
        dataline = Dataline.create! row.to_hash.slice(*allowed_params)
        dataline.save!
      rescue
        next
      end
    end
  end
    
	def self.new_odo(bike)
    if !bike.datalines.all.present? || bike.odo > bike.current_odo
      dataline = Dataline.new
      dataline.value = bike.odo
      dataline.recorded_at = Time.now
      dataline.datatype = 'odometer'
      dataline.bike_id = bike.id
      dataline.save!
    end
  end
  
end
