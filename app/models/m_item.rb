class MItem < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
	belongs_to :bike
  has_paper_trail

  validates :name, :presence => true
  validates :name, :length => { :maximum => 30 }
  validates :notes, :length => { :maximum => 200 }
  
  validates_numericality_of :odo, :greater_than_or_equal_to => 0, :unless => proc{|obj| obj.odo.blank?}
  #validates_date :date, :unless => proc{|obj| obj.payment.blank?}


    validate :odo_or_duedate

    def odo_or_duedate
      if odo.blank? && date.blank?
        errors.add_to_base("Specify a due date and/or odometer value.")
      end
    end

	  def overdue?
    	((odo && odo < bike.current_odo) || (date && date < Date.today)) && completed_at == nil
  	end

  	def dueSoon?
    	((odo && odo < bike.odoGuess) || (date && date < (Date.today + 1.week))) && !overdue? && completed_at == nil
  	end

  	def self.import(file)
      allowed_params = ["date","name","odo","notes"]
	    CSV.foreach(file.path, :headers => true) do |row|
      
	  	  next if row.header_row?
          begin
            m_item = MItem.create! row.to_hash.slice(*allowed_params)
            m_item.save!
          rescue
            next
          end
	      end
	    end

    end
