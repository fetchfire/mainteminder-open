class Bike < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
	belongs_to :user
  has_many :m_items
  has_many :datalines
  has_paper_trail

  validates :year, :numericality => { :greater_than_or_equal_to => 1000, :less_than_or_equal_to => 9999 }
  validates :name, :presence => true
  validates :name, :length => { :maximum => 20 }
  validates :plate, :length => { :maximum => 20 }
  validates :colour, :length => { :maximum => 20 }
  validates :odo, :numericality => { :greater_than_or_equal_to => 0}
  
  def current_odo
  	odo = datalines.where(:datatype => "odometer").order("recorded_at DESC").first
    if odo == nil 
      return 1
    else
      return odo.value
    end
  end

  def overcount
  	count = 0
    	m_items.each do  |m_item| 

			if m_item.overdue? 
				count = count + 1
			end
		end
	return count
  end

  def dueSoonCount
    count = 0
      m_items.each do  |m_item| 

      if m_item.dueSoon? 
        count = count + 1
      end
    end
  return count
  end

  def odoGuess
      # Based on average of last 3 weeks odo readings.
      accuracy = 10 #higher the number better the guess but slower the read
      prev_line = nil
      odo_per_day = 0
      current = 0
      last_odo = 0
      last_odo_date = nil
      count = 0
      datalines.where(:datatype => "odometer").order("recorded_at DESC").first(accuracy).each do |line| 
        if count >= 1
          dif = line.value - prev_line.value
          time_dif = (line.recorded_at - prev_line.recorded_at).to_i / 1.day
          odo_per_day = odo_per_day + (dif / time_dif)
        else
          last_odo = line.value
          last_odo_date = line.recorded_at
        end
        count = count + 1
        prev_line = line
      end
      odo_per_day = odo_per_day / (count)
      p odo_per_day
      days_since = ((Time.now - last_odo_date).to_i / 1.day)
      if days_since < 1
        return last_odo.to_i 
      else
        return (last_odo + (odo_per_day * days_since)).to_i 
      end
  end
  
  def self.migrate_odo
    bikes = Bike.all
    bikes.each do |bike|
		  odo = bike.odo
      if odo != nil
        dataline = Dataline.create!
        dataline.value = odo
        dataline.recorded_at = Time.now
        dataline.datatype = 'odometer'
        dataline.bike_id = bike.id
        dataline.save!
      end
		  odo = bike.version_at(1.weeks.ago).odo
      if odo != nil
        dataline = Dataline.create!
        dataline.value = odo
        dataline.recorded_at = Time.now.ago(1.weeks)
        dataline.datatype = 'odometer'
        dataline.bike_id = bike.id
        dataline.save!
      end
		  odo = bike.version_at(2.weeks.ago).odo
      if odo != nil
        dataline = Dataline.create!
        dataline.value = odo
        dataline.recorded_at = Time.now.ago(2.weeks)
        dataline.datatype = 'odometer'
        dataline.bike_id = bike.id
        dataline.save!
      end
		  odo = bike.version_at(3.weeks.ago).odo
      if odo != nil
        dataline = Dataline.create!
        dataline.value = odo
        dataline.recorded_at = Time.now.ago(3.weeks)
        dataline.datatype = 'odometer'
        dataline.bike_id = bike.id
        dataline.save!
      end
    end
  end

end
