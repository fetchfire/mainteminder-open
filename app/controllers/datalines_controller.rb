class DatalinesController < ApplicationController

  # POST /datalines
  # POST /datalines.json
  # only for odometer
  def create
    @bike = current_user.bikes.find(params[:bike_id])
    @dataline = @bike.datalines.build(dataline_params)

    respond_to do |format|
      if @dataline.save
        format.html { redirect_to bikes_path, notice: 'Odometer updated!' }
        format.json { render json: @dataline, status: :created, location: @dataline }
      else
        error = @dataline.errors
        format.html { redirect_to bikes_path, notice: 'Error: Odometer must be a valid number.'}
        format.json { render json: @dataline.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def import
    @bike = current_user.bikes.find(params[:bike_id])
    @file = params[:file]
    if @file != nil
      @bike.m_items.import(@file)
      redirect_to @bike, notice: "Import Complete."
    else
      redirect_to @bike, notice: "Import failed, please check file."
    end
  end
  
  # def graph
#       @bike = current_user.bikes.find(params[:bike_id])
#       @data1 = params[:data1]
#       @start = params[:start]
#       @finish = params[:finish]
#       
#       @series1 = current_user.bikes.map do |bike|
#             {
#               :key => @data1,
#               :values => bike.datalines.value.where(:bike_id => bike AND :datatype => @data1 AND Time.now >= @start AND Time.now <= @finish).order("recorded_at DESC")
#             }
 # end

 
  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def dataline_params
      params.require(:dataline).permit(:integer, :recorded_at, :datatype, :value)
    end
end
