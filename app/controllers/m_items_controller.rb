class MItemsController < ApplicationController

  # GET /m_items/new
  # GET /m_items/new.json
  def new
    @bike = current_user.bikes.find(params[:bike_id])
    @m_item = MItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @m_item }
    end
  end

  # POST /m_items
  # POST /m_items.json
  def create
    @bike = current_user.bikes.find(params[:bike_id])
    @m_item = @bike.m_items.build(m_item_params)

    respond_to do |format|
      if @m_item.save
        format.html { redirect_to @bike }
      else
        format.html { render action: "new" }
        format.json { render json: @m_item.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /m_items/1
  # DELETE /m_items/1.json
  def destroy
    @m_item = current_user.m_items.find(params[:id])
    @m_item.destroy
    redirect_to :back, notice: "Item deleted #{undo_link}"
  end
  
  def complete
    @m_item = current_user.m_items.find(params[:m_item_id])
    @bike = current_user.bikes.find(@m_item.bike_id)
    @m_item.completed_at = DateTime.now
    @m_item.completed_at_odo = @bike.odo
    @m_item.save
    redirect_to :back, notice: "Item marked complete #{undo_link}"
  end

  def import
    @bike = current_user.bikes.find(params[:bike_id])
    @file = params[:file]
    if @file != nil
      @bike.m_items.import(@file)
      redirect_to @bike, notice: "Import Complete."
    else
      redirect_to @bike, notice: "Import failed, please check file."
    end
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@m_item.versions.scoped.last), :method => :post)
  end
  
  private
      # Use callbacks to share common setup or constraints between actions.
      def set_m_item
        @m_item = MItem.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def m_item_params
        params.require(:m_item).permit(:date, :name, :odo, :notes, :completed_at, :completed_at_odo)
      end

end
