class SessionsController < ApplicationController

skip_before_filter(:authenticate_user!)

  def new
  	if AdminObject.find_by_name("welcome_text").present?
  		@welcome_text = AdminObject.find_by_name("welcome_text").body
  	else
  		@welcome_text = ""
  	end
end

def create
	# raise request.env["omniauth.auth"].to_yaml
  auth = request.env["omniauth.auth"]
  user = User.find_by_provider_and_uid(auth["provider"], auth["uid"]) || User.create_with_omniauth(auth)
  	if user.disabled_at == nil
      if user.provider != "identity"
		    user.update_picture(auth['info']['image'])
      end
		session[:user_id] = user.id
		redirect_to root_url, :notice => "Signed in!"
	else
	  	redirect_to new_session_path, :notice => "Your account has been deleted, please contact us for more information."
	end
end

def failure
	 redirect_to login_path, :notice => "Your email or password were incorrect."
end

def destroy
  session[:user_id] = nil
  redirect_to new_session_path, :notice => "Logged out!"
end
end