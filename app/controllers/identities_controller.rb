class IdentitiesController < ApplicationController
  skip_before_filter(:authenticate_user!)
  def new
    @identity = env['omniauth.identity']
  end
end
