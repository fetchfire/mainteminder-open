class PagesController < ApplicationController

	skip_before_filter(:authenticate_user!)

	def about
    if AdminObject.find_by_name("about_main").present?
      @about_main = AdminObject.find_by_name("about_main").body
    else
      @about_main = ""
  	end
  end

  	def changelog
      @objects = AdminObject.all

      respond_to do |format|
        format.html # changelog.html.erb
        format.json { render json: @objects }
      end
  	end

  	def faqs
      @objects = AdminObject.all

      respond_to do |format|
        format.html # faqs.html.erb
        format.json { render json: @objects }
      end
  	end
    
  	def privacy
      if AdminObject.find_by_name("privacy_policy").present?
        @privacy_policy = AdminObject.find_by_name("privacy_policy").body
      else
        @privacy_policy = ""
    	end
      respond_to do |format|
        format.html # privacy.html.erb
      end
  	end
    
  	def login
      if !current_user
      respond_to do |format|
        format.html # faqs.html.erb
      end
    else
      #format.html { render action: "index", notice: "You are already logged in."}
    end
  	end


end
