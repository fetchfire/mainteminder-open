class ApplicationController < ActionController::Base
  protect_from_forgery
  
 	helper_method :current_user
  before_filter :authenticate_user!

	private

	def current_user
	  @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
	end

	helper_method :user_logged_in?
  def user_logged_in?
    current_user != nil
  end

  helper_method :user_enabled?
  def user_enabled?
    current_user.disabled_at == nil
  end

  private
  def authenticate_user!
   if !user_logged_in? || !user_enabled?
     session[:redirect] = request.fullpath
     redirect_to new_session_path
   end
  end

end
