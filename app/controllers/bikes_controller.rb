class BikesController < ApplicationController
  # GET /bikes
  # GET /bikes.json
  def index
    @bikes = Bike.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bikes }
    end
  end

  # GET /bikes/1
  # GET /bikes/1.json
  def show
    @bike = current_user.bikes.find(params[:id])
    @m_items = @bike.m_items.order("date ASC")

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bike }
    end
  end
  
  def dash
    @bike = current_user.bikes.find(params[:bike_id])
    @datalines = @bike.datalines.order("recorded_at DESC")

    respond_to do |format|
      format.html # dash.html.erb
      format.json { render json: @bike }
    end
  end
  
  # def graph
#       @bike = current_user.bikes.find(params[:bike_id])
#       @data1 = params[:data1]
#       @start = params[:start]
#       @finish = params[:finish]
#       
#       @series1 = current_user.bikes.map do |bike|
#             {
#               :key => @data1,
#               :values => bike.datalines.value.where(:bike_id => bike).order("recorded_at DESC")
#             }
#           end
#   end
  
  def report
    @bike = current_user.bikes.find(params[:bike_id])
    @m_items = @bike.m_items.order("date ASC")
    respond_to do |format|
      format.html {}
      format.json { render json: @bike }
    end
  end
  
  def import
  	if AdminObject.find_by_name("import_text").present?
  		@import_text = AdminObject.find_by_name("import_text").body
  	else
  		@import_text = ""
  	end
    @bike = current_user.bikes.find(params[:bike_id])
    respond_to do |format|
      format.html # import.html.erb
      format.json { render json: @bike }
    end
  end

  # GET /bikes/new
  # GET /bikes/new.json
  def new
    if ( !current_user.limit.present? || !current_user.limit_reached )
      @bike = Bike.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @bike }
      end
    else
      format.html { render action: "index", notice: "You have already reached your vehicle limit."}
    end
  end

  # GET /bikes/1/edit
  def edit
    @bike = current_user.bikes.find(params[:id])
  end

  # POST /bikes
  # POST /bikes.json
  def create
    @bike = current_user.bikes.build(bike_params)
    
    # @bike = Bike.new(bike_params)
#     @bike.user_id = current_user.id

    respond_to do |format|
      if @bike.save
        @bike.datalines.new_odo(@bike)
        format.html { redirect_to @bike, notice: "Vehicle was successfully created."}
        format.json { render json: @bike, status: :created, location: @bike }
      else
        format.html { render action: "new" }
        format.json { render json: @bike.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bikes/1
  # PUT /bikes/1.json
  def update
    @bike = current_user.bikes.find(params[:id])

    respond_to do |format|
      if @bike.update_attributes(bike_params)
        @bike.datalines.new_odo(@bike)
        format.html { redirect_to @bike, notice: "Vehicle was successfully updated. #{undo_link}" }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bike.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bikes/1
  # DELETE /bikes/1.json
  def destroy
    @bike = current_user.bikes.find(params[:id])
    @bike.destroy

    respond_to do |format|
      format.html { redirect_to bikes_url, notice: "Vehicle was successfully deleted. #{undo_link}" }
      format.json { head :no_content }
    end
  end


  def undo_link
    view_context.link_to("undo", revert_version_path(@bike.versions.scoped.last), :method => :post)
  end

  private
      # Use callbacks to share common setup or constraints between actions.
      def set_bike
        @bike = Bike.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def bike_params
        params.require(:bike).permit(:name, :odo, :plate, :colour, :year, :picurl)
      end

end
