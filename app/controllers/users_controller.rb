class UsersController < ApplicationController

	skip_before_filter :authenticate_user!, :only => [:create, :new]

	def new
	  @user = User.new
	end

	def create
	  @user = User.new(user_params)
	  if @user.save
	    redirect_to new_session_path, :notice => "Signed up!"
	  else
	    render "new"
	  end
	end

	def show
    	@user = current_user

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @user }
	    end
	  end

	def index
		unless current_user.is_admin?
	      session[:redirect] = request.fullpath
	      redirect_to bikes_path
	    end

    	@users = User.all

	    respond_to do |format|
	      format.html # index.html.erb
	      format.json { render json: @user }
	    end
	  end

	def destroy
		@user = current_user
	  	@user.disabled_at = Time.zone.now
	  	@user.save
  		redirect_to new_session_path, :notice => "Account deleted, feel free to come back!"
	end

	def destroy_by_admin
		if current_user.is_admin?
			@user = User.find(params[:user_id])
		  	@user.disabled_at = Time.zone.now
		  	@user.save
	  		redirect_to users_path, :notice => "Account disabled"
	  	end
	end
  
	def enable_by_admin
		if current_user.is_admin?
			@user = User.find(params[:user_id])
		  	@user.disabled_at = nil
		  	@user.save
	  		redirect_to users_path, :notice => "Account enabled"
	  	end
	end

	def update
		@user = current_user
	    respond_to do |format|
	      if @user.update_attributes(user_params)
	        format.html { redirect_to bikes_path, notice: "User was successfully updated." }
	        format.json { head :no_content }
	      else
	        format.html { render action: "show" }
	        format.json { render json: @user.errors, status: :unprocessable_entity }
	      end
	    end
	  end

	def set_admin
		if current_user.is_admin?
			@user = User.find(params[:user_id])
			@user.is_admin = true
			@user.save
			redirect_to users_path, :notice => "User #" + @user.id.to_s + " is now an admin"
    else
      redirect_to users_path, :notice => "Nope! Only admins have such power."
		end
	end

	def unset_admin
		if current_user.is_admin?
			@user = User.find(params[:user_id])
			@user.is_admin = false
			@user.save
			redirect_to users_path, :notice => "User #" + @user.id.to_s + " is no longer an admin"
    else
      redirect_to users_path, :notice => "Nope! Only admins have such power."
		end
	end

	def set_tester
		if current_user.is_admin?
			@user = User.find(params[:user_id])
			@user.is_tester = true
			@user.save
			redirect_to users_path, :notice => "User #" + @user.id.to_s + " can now access test"
    else
      redirect_to users_path, :notice => "Nope! Only admins have such power."
		end
	end

	def unset_tester
		if current_user.is_admin?
			@user = User.find(params[:user_id])
			@user.is_tester = false
			@user.save
			redirect_to users_path, :notice => "User #" + @user.id.to_s + " can no longer access test"
    else
      redirect_to users_path, :notice => "Nope! Only admins have such power."
		end
	end
  
  private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        params.require(:user).permit(:email, :password, :password_confirmation, 
        :uid, :provider, :name, :avatar, :guess_on, :is_subscribed)
      end

end