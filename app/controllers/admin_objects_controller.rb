class AdminObjectsController < ApplicationController
  
  before_filter :authenticate_admin!

  def authenticate_admin!
    unless current_user.is_admin?
      session[:redirect] = request.fullpath
      redirect_to bikes_path
    end
  end

  def index
    @admin_objects = AdminObject.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_objects }
    end
  end

  def changes
    @admin_objects = AdminObject.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_objects }
    end
  end

  def faqs
    @admin_objects = AdminObject.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_objects }
    end
  end

  def text
    @admin_objects = AdminObject.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_objects }
    end
  end
  
  def stats
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /admin_objects/1
  # GET /admin_objects/1.json
  def show
    @admin_object = AdminObject.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin_object }
    end
  end

  # GET /admin_objects/new
  # GET /admin_objects/new.json
  def new
    @admin_object = AdminObject.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_object }
    end
  end

  # GET /admin_objects/1/edit
  def edit
    @admin_object = AdminObject.find(params[:id])
  end

  # POST /admin_objects
  # POST /admin_objects.json
  def create
    @admin_object = AdminObject.new(admin_object_params)

    respond_to do |format|
      if @admin_object.save
        format.html { redirect_to @admin_object, notice: 'Admin object was successfully created.' }
        format.json { render json: @admin_object, status: :created, location: @admin_object }
      else
        format.html { render action: "new" }
        format.json { render json: @admin_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin_objects/1
  # PUT /admin_objects/1.json
  def update
    @admin_object = AdminObject.find(params[:id])

    respond_to do |format|
      if @admin_object.update_attributes!(admin_object_params)
        format.html { redirect_to @admin_object, notice: 'Admin object was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin_objects/1
  # DELETE /admin_objects/1.json
  def destroy
    @admin_object = AdminObject.find(params[:id])
    @admin_object.destroy

    respond_to do |format|
      format.html { redirect_to admin_objects_url }
      format.json { head :no_content }
    end
  end
  
  private
      # Use callbacks to share common setup or constraints between actions.
      def set_admin_object
        @admin_object = AdminObject.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def admin_object_params
        params.require(:admin_object).permit(:name, :body, :date_created, :object_type, :priority, :answer)
      end
end
