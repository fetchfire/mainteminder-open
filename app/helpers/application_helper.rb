module ApplicationHelper

	def markdown(text)
	  	extensions = { :fenced_code_blocks => true }
    	render_opts = { :filter_html => true, :no_styles => true, :safe_links_only => true }
    	Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(render_opts), extensions).render(text).html_safe
	end


end
